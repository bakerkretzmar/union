$(function() {
  if ($(document).scrollTop() > 50) {
    $('header#masthead').addClass('shrink');
    $('nav#site-navigation').addClass('shrink');
  } else {
    $('header#masthead').removeClass('shrink');
    $('nav#site-navigation').removeClass('shrink');
  };
});

$(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('header#masthead').addClass('shrink');
    $('nav#site-navigation').addClass('shrink');
  } else {
    $('header#masthead').removeClass('shrink');
    $('nav#site-navigation').removeClass('shrink');
  };
});

$(function() {
  var scrollStart = 94;
  var footerStart = $('footer.site-footer').offset().top;
  var sidebarEndPosition = $(document).scrollTop() + $('aside.widget-area').outerHeight() + scrollStart;

  if ($(document).scrollTop() > scrollStart) {
    $('aside.widget-area').addClass('floating');
  } else {
    $('aside.widget-area').removeClass('floating');
  };

  if (sidebarEndPosition >= footerStart) {
    $('aside.widget-area').addClass('pushed');
    $('aside.widget-area').removeClass('floating');
  } else {
    $('aside.widget-area').removeClass('pushed');
  };
});

$(window).scroll(function() {
  var scrollStart = 94;
  var footerStart = $('footer.site-footer').offset().top;
  var sidebarEndPosition = $(document).scrollTop() + $('aside.widget-area').outerHeight() + scrollStart;

  if ($(document).scrollTop() > scrollStart) {
    $('aside.widget-area').addClass('floating');
  } else {
    $('aside.widget-area').removeClass('floating');
  };

  if (sidebarEndPosition >= footerStart) {
    $('aside.widget-area').addClass('pushed');
    $('aside.widget-area').removeClass('floating');
  } else {
    $('aside.widget-area').removeClass('pushed');
  };
});

$(document).ready(function blink() {
  $('span#dot').fadeToggle( 1000, "swing", blink);
});

$(function() {
  if ( $(window).width() <= 801 ) {
    $('.menu-toggle').click(function() {
      $('header#masthead .menu-main-menu-container').toggle();
    });
  } else {
    $('header#masthead .menu-main-menu-container').css('display', 'block');
  };
});
