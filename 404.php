<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Union
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h4 class="page-title">George Cooper Memorial 404 Page.</h4>
				</header><!-- .page-header -->

				<div class="page-content">
					<p>That URL doesn’t point to a page on this site. Try one of the links below, or a search.</p>

					<?php get_template_part( 'parts/searchform' ); ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
