<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Union
 */

// if ( ! is_active_sidebar( 'sidebar-1' ) ) {
// 	return;
// }
?>

<aside id="secondary" class="widget-area" role="complementary">

  <div id="search" class="widget widget_search">
    <?php get_template_part( 'parts/searchform' ); ?>
  </div>

  <?php dynamic_sidebar( 'post_sidebar' ); ?>

</aside><!-- #secondary -->
