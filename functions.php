<?php
/**
 * Union functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Union
 */

if ( ! function_exists( 'union_setup' ) ) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   */
  function union_setup() {
  	// Add default posts and comments RSS feed links to head.
  	add_theme_support( 'automatic-feed-links' );

  	// Let WordPress manage the document title.
  	add_theme_support( 'title-tag' );

    // Enable support for Post Thumbnails on posts and pages.
    // https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
  	add_theme_support( 'post-thumbnails' );

    // Enable custom TinyMCE editor styles
    add_editor_style();

  	// This theme uses wp_nav_menu() in two locations.
  	register_nav_menus( array(
  		'primary' => esc_html__( 'Primary', 'union' ),
      'secondary' => esc_html__( 'Secondary', 'union' ),
  	) );

    // Switch default core markup for search form, comment form, and comments to output valid HTML5.
  	add_theme_support( 'html5', array(
  		'search-form',
  		'comment-form',
  		'comment-list',
  		'gallery',
  		'caption',
  	) );
  }
endif;
add_action( 'after_setup_theme', 'union_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function union_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Post Sidebar', 'union' ),
		'id'            => 'post_sidebar',
		'description'   => esc_html__( 'Sidebar to display on post pages, such as the live blogs.', 'union' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
  register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Area', 'union' ),
		'id'            => 'footer_widget_area',
		'description'   => esc_html__( 'Widget area for the footer.', 'union' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'union_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function union_scripts() {
	wp_enqueue_style( 'union-style', get_stylesheet_uri() );

	// wp_enqueue_script( 'union-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'union-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

  wp_enqueue_script( 'union-jquery', get_template_directory_uri() . '/js/jquery-3.1.1.min.js', array(), '', false );

  wp_enqueue_script( 'union-main', get_template_directory_uri() . '/js/main.js', array(), '', true );

  wp_enqueue_script( 'union-slick', get_template_directory_uri() . '/js/slick.min.js', array(), '', false );
}
add_action( 'wp_enqueue_scripts', 'union_scripts' );

// Set content_width
if ( ! isset( $content_width ) ) {
	$content_width = 6000;
}

// img unautop
function img_unautop($pee) {
    $pee = preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '$1', $pee);
    return $pee;
}
add_filter( 'the_content', 'img_unautop', 30 );

/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function union_meta() {

  $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date( 'D M j, Y' ) )
	);

  $updated_string = '<time class="entry-date updated" datetime="%1$s">%2$s</time>';

  $updated_string = sprintf( $updated_string,
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date( 'g:i A, M j, Y' ) )
	);

  $categories = get_the_category_list( esc_html__( ', ', 'union' ) );

  $live = '';
  if ( ( ( current_time( 'timestamp' ) - get_the_modified_date( 'U' ) ) < 600 ) && ( has_category( "Live Blog" ) ) ) {
    $live = '<span id="live">&nbsp;&nbsp;LIVE <span id="dot">•</span></span>';
  }

	echo '<p class="meta">Posted ' . $time_string . ', in ' . $categories . '. Last updated ' . $updated_string . '.' . $live . '</p>'; // WPCS: XSS OK.

  $tags_list = get_the_tag_list();
  if ( $tags_list ) {
    printf( '<span class="tags-links">' . esc_html__( '%1$s', 'union' ) . '</span>', $tags_list ); // WPCS: XSS OK.
  }

}

/**
 * Custom shortcode function for populating lists of child pages.
 */
function child_pages_function() {
  $parent = get_the_ID();
  $query = new WP_Query(
    array(
      'post_parent' => $parent,
      'post_type'   => 'page',
      'post_status' => 'published',
      'order'       => 'ASC',
      'orderby'     => 'menu_order'
    ));
  if ( $query->have_posts() ) {
  	while ( $query->have_posts() ) {
  		$query->the_post();
  		echo '<p><a href="' . get_permalink() . '">' . get_the_title() . '</a></p>';
  	}
    wp_reset_postdata();
  } else {
  	echo '<p>No child pages found.</p>';
  }
}
function register_shortcodes(){
   add_shortcode('child-pages', 'child_pages_function');
}
add_action( 'init', 'register_shortcodes');

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function union_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', bloginfo( 'pingback_url' ), '">';
	}
}
add_action( 'wp_head', 'union_pingback_header' );
