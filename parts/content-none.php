<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Union
 */

?>

<section class="no-results not-found">
	<header class="page-header">
		<h4 class="page-title"><?php esc_html_e( 'Nothing Found', 'union' ); ?></h4>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here.</a>', 'union' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Sorry, nothing matched that search. Try again with some different keywords.', 'union' ); ?></p>
			<?php
				get_template_part( 'parts/searchform' );

		else : ?>

			<p><?php esc_html_e( 'It seems we can’t find what you’re looking for. Try a search.', 'union' ); ?></p>
			<?php
				get_template_part( 'parts/searchform' );

		endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
