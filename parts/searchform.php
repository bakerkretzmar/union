<?php
/**
 * The template part for displaying the search form.
 *
 * @package Abstrakt
 */

?>

<form role="search" method="get" class="search-form" action="http://localhost:8888/ksu/">
  <input type="search" class="search-field" placeholder="Search..." value="" name="s" />
  <label>
    <input type="submit" />
    <svg version="1.1" id="search" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.4 14.4" enable-background="new 0 0 14.4 14.4;" xml:space="preserve">
      <path id="search" fill="none" stroke="#000" stroke-width="2" stroke-miterlimit="10" d="M10.086,5.8684C9.8912,8.1676,7.8694,9.8736,5.5701,9.6788S1.5649,7.4621,1.7597,5.1629
    	s2.2166-4.0052,4.5159-3.8104S10.2808,3.5691,10.086,5.8684z M8.6138,8.7048l4.0416,4.7899"/>
    </svg>
  </label>
</form>
