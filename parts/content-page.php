<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Union
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
    <?php if ( has_post_thumbnail() ) : ?>
      <figure class="wp-caption">
        <?php the_post_thumbnail( 'full' ); ?>
        <figcaption class="wp-caption-text"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></figcaption>
      </figure>
    <?php endif; ?>
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
