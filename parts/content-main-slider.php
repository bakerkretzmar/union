<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Union
 */

?>

<div class="slick-main-slider">
  <?php
  $recent_posts = wp_get_recent_posts( array(
    'numberposts' => 5,
    'tag'         => 'featured',
    'post_status' => 'publish'
  ));
  foreach($recent_posts as $post) : ?>
    <div>
      <?php echo get_the_post_thumbnail( $post['ID'], 'full' ); ?>
      <div class="caption">
        <?php $categories = get_the_category_list( esc_html__( ', ', 'union' ), '', $post['ID'] ); ?>
        <h1 class="entry-title"><a href="<?php echo get_the_permalink( $post ['ID'] ); ?>" rel="bookmark"><?php echo get_the_title( $post['ID'] ); ?></a></h1>
        <p class="entry-meta"><?php echo get_the_date( 'D M j, Y', $post['ID'] ); ?>, in <?php echo get_the_category_list( ', ', '', $post['ID'] ); ?><?php if ( ( ( current_time( 'timestamp' ) - get_the_modified_date( 'U', $post['ID'] ) ) < 600 ) && ( has_category( "Live Blog", $post['ID'] ) ) ) { echo ' | <span id="live">&nbsp;LIVE <span id="dot">•</span></span>'; }; ?></p>
      </div>
    </div>
  <?php endforeach; wp_reset_query(); ?>
</div>
