<?php
/**
 * The template for displaying the page with the slug 'home' (the front page).
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Union
 */

get_header(); ?>

  <?php get_template_part( 'parts/content', 'main-slider' ); ?>

  <script type="text/javascript">
    $('.slick-main-slider').slick({
      autoplay: true,
      autoplaySpeed: 4200,
      speed: 600,
      dots: true,
      pauseOnHover: false
    });
  </script>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'parts/content', 'page' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
