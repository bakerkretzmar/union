<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Union
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="footer-widget-area">
      <?php dynamic_sidebar( 'footer_widget_area' ); ?>
    </div>

    <div class="site-info">
      <p id="copyright">© <?php echo current_time( 'Y' ) ?> King’s Students’ Union</p><p id="cfs"><span>&nbsp;|&nbsp;</span><a href="http://cfs-fcee.ca/">Canadian Federation of Students</a> Local 11</p>
			<p id="powered">Powered by <a href="http://wordpress.org/">WordPress</a><span>&nbsp;|&nbsp;</span></p><p id="theme-by"><a href="http://bakerkretzmar.ca/" rel="designer">Union</a> theme</p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<!-- Trackers! -->
<script type="text/javascript">
  var _mfq = _mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/6b50fadc-b1ad-4e86-9bc6-d361c7fa350b.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>
<script type="text/javascript" id="inspectletjs">
  window.__insp = window.__insp || [];
  __insp.push(['wid', 648370304]);
  (function() {
    function ldinsp(){if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
    setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
  })();
</script>

</body>
</html>
